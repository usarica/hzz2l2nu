#ifndef HZZ2L2NU_INCLUDE_KFACTORCORRECTION_H_
#define HZZ2L2NU_INCLUDE_KFACTORCORRECTION_H_

#include <WeightBase.h>

#include <filesystem>

#include <TSpline.h>
#include <TTreeReaderArray.h>

#include <Dataset.h>
#include <Options.h>

/**
 * \brief Computes k factor for gluon fusion production
 *
 * The k factor is computed if the per-dataset configuration contains parameter
 * "k_factor" and its value is "ggF". Otherwise a weight of 1 is returned for
 * every event.
 */
class KFactorCorrection : public WeightBase {
 public:
  KFactorCorrection(Dataset &dataset, Options const &options);

  /**
   * \brief Computes mass of generator-level Higgs boson
   *
   * The Higgs boson is reconstructed from generarol-level leptons.
   */
  double HiggsMass() const;

  /**
   * \brief Computes and returns the nominal k factor for the current event
   *
   * The k factor is 1 if the correction is disabled.
   */
  double NominalWeight() const override;

  double WeightRenScaleDown() const;
  double WeightRenScaleUp() const;
  double WeightFacScaleDown() const;
  double WeightFacScaleUp() const;
  double WeightPdfDown() const;
  double WeightPdfUp() const;
  double WeightAsMzDown() const;
  double WeightAsMzUp() const;

  int NumVariations() const override {return 9;}
  double RelWeight(int variation) const override;
  std::string_view VariationName(int variation) const override;

 private:
  bool enabled_;

  /// The k factor as a function of the mass of the Higgs boson
  std::vector<TSpline3> kfactors_;

  mutable TTreeReaderArray<float> genPartPt_, genPartEta_, genPartPhi_,
    genPartMass_;
  mutable TTreeReaderArray<int> genPartStatus_, genPartStatusFlags_;
};

#endif  // HZZ2L2NU_INCLUDE_KFACTORCORRECTION_H_

