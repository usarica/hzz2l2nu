#!/bin/bash


####################################################################
############################   Options   ###########################
####################################################################
function load_options() {
  #pipefail: the return value of a pipeline is the status of the last command to exit with a non-zero status, or zero if no command exited with a non-zero status
  #this is needed for retry function to work properly
  set -o pipefail
  
  SLEEP_TIME_QSTAT=60 # in seconds
  SLEEP_TIME=60 #in seconds
  CHECK_TIME=6 #check qstat every CHECK_TIME*SLEEP_TIME_QSTAT
  
  # Colors
  BLUE='\033[1;34m'
  RED='\033[1;31m'
  GREEN='\033[1;32m'
  YEL='\033[1;33m'
  MAG='\033[1;35m'
  DEF='\033[0;m'
  
  # Nice printing
  I="$GREEN[INFO] $DEF"
  E="$RED[ERROR] $DEF"
  W="$YEL[WARN] $DEF"

  # Paths definition
  base_path="$HZZ2L2NU_BASE/"
  instrMET_path="WeightsAndDatadriven/InstrMET/"
  full_path="${base_path}${instrMET_path}"
  
  launchAnalysis_step1="${full_path}launchAnalysis_doClosureTest_step1.sh"
  launchAnalysis_step3="${full_path}launchAnalysis_doClosureTest_step3.sh"
  launchAnalysis_step6="${full_path}launchAnalysis_doClosureTest_step6.sh"
  listSamplesToRun_HZZ="${full_path}listSamplesToRun_diLeptonMCOnly.txt"
  listSamplesToRun_Photon="${full_path}listSamplesToRun_InstrMET_GJetsAndQCDMCOnly.txt"

  # The various suffixes
  suffix_step1_HZZ="closureTest_DiLeptonMC" #DO NOT CHANGE THIS, IT IS USED BY THE MACRO TO COMPUTE WEIGHTS
  suffix_step1_InstrMET="closureTest_PhotonMC_NoWeight" #DO NOT CHANGE THIS, IT IS USED BY THE MACRO TO COMPUTE WEIGHTS
  suffix_step3_InstrMET="closureTest_PhotonMC_NVtx_WeightApplied" #DO NOT CHANGE THIS, IT IS USED BY THE MACRO TO COMPUTE WEIGHTS
  suffix_step6_InstrMET="closureTest_PhotonMC_AllWeightsAndLineshapeApplied" #DO NOT CHANGE THIS, IT IS USED BY THE MACRO TO COMPUTE WEIGHTS

  # Log file
  logFile="${base_path}OUTPUTS/fullAnalysis_doClosureTest.$(current_time).log"
  
  # Running options (default)
  express=""   # set to '--express' to run on express all the time
  noLocalCopy="" # set to '--noLocalCopy' to NOT do a local copy and stream the ROOT files
  step="all" # by default, run on all 5 steps of the analysis (0, 1, 2, 3, 4)
  addQCD=0 #set to one if you also want to take QCD in the closure test

}


####################################################################
##########################   Help (usage)   ########################
####################################################################
function usage() {
  printf "$BLUE NAME $DEF \n\tdoClosureTest.sh - All-in-one launcher to perform the closure test for the Instr.MET\n"
  printf "\n\t%-5b  %-40b\n"  "$MAG -h/-help/--help $DEF"            "print this help"
  printf "\n\t%-5b  %-40b\n"  "$RED all $DEF (default)"  "${stepName[all]}"
  printf "\t%-5b  %-40b\n"  "$RED 0 $DEF"              "${stepName[0]}"
  printf "\t%-5b  %-40b\n"  "$RED 1 $DEF"              "${stepName[1]}"
  printf "\t%-5b  %-40b\n"  "$RED 2 $DEF"              "${stepName[2]}"
  printf "\t%-5b  %-40b\n"  "$RED 3 $DEF"              "${stepName[3]}"
  printf "\t%-5b  %-40b\n"  "$RED 4 $DEF"              "${stepName[4]}"
  printf "\t%-5b  %-40b\n"  "$RED 5 $DEF"              "${stepName[5]}"
  printf "\t%-5b  %-40b\n"  "$RED 6 $DEF"              "${stepName[6]}"
  printf "\t%-5b  %-40b\n"  "$RED 7 $DEF"              "${stepName[7]}"
}

####################################################################
####################   Miscellaneous Functions   ###################
####################################################################

function current_time() {
  local datestamp=$(date  +%Y-%m-%d-%H:%M:%S)
  echo "$datestamp"
}

function send_mail(){
  export LD_LIBRARY_PATH="/lib64/:$LD_LIBRARY_PATH"
  mailAddress=$(grep $USER ${base_path}Tools/userInfo.db | awk  '{print $2}')
  if [[ $mailAddress == *"@"* ]]; then
    echo -e "$I Mail address found in the db : $mailAddress"
  elif [[ $mailAddress == "no" ]]; then
    echo -e "$I User asked to not received emails"
    return 0
  else
    mailAddress=$(ldapsearch -LLL -x uid=$USER mail | sed -n 's/^[ \t]*mail:[\t]*\(.*\)/\1/p')
  fi
  sed -i "1s/^/Subject: Jobs for H->ZZ $(current_time)\n/" $logFile
  sendmail $mailAddress < $logFile
}

function check_running_process() {
  #Kill the process if it is running already...
  me=$(basename $0);
  for pid in $(pidof -x $me); do
      if [ $pid != $$ ]; then
        echo -e "$E An instance of this script is still running in the background. Should I kill it before launching the script? [N/y]?"
        read answer
        if [[ $answer == "y" ]]; then
          kill -9 $pid
          echo -e "$I Instance $pid has been killed"
        fi
      fi
  done

}

# Retries a command on failure.
# $1 - the max number of attempts
# $2... - the command to run
function retry() {
    baseWaitingTime=10
    local -r -i max_attempts="$1"; shift
    local -r cmd="$@"
    local -i attempt_num=1

    until $cmd
    do
        if (( attempt_num == max_attempts ))
        then
            echo -e "$E Attempt $attempt_num failed and there are no more attempts left!" >&2
            exit 5
        else
            echo -e "$W Attempt $attempt_num failed! Trying again in $(( baseWaitingTime * attempt_num )) seconds..." >&2
            sleep $(( baseWaitingTime * attempt_num ))
            (( attempt_num++ ))
        fi
    done
}


####################################################################
#######################   Introduction Text   ######################
####################################################################
function print_introduction_text(){
  # The text
  echo -e "$I For $YEL help $DEF just add $YEL -h $DEF"
  echo -e "$I Don't forget only one instance of this script can run at the same time."
  echo -e "$I If you need to kill the script, just relaunch this script: it will ask you automatically if you want to kill the running script."
  echo -e "$I You have chosen the following options:"
  echo -e "\t $MAG step: ${step}, i.e. ${stepName[$step]} $DEF"
  echo -e "\t $MAG express: $express $DEF"
  echo -e "\t $MAG noLocalCopy: $noLocalCopy $DEF"
  echo -e "\t $MAG addQCD: $addQCD $DEF"

}

####################################################################
#######################   Prepare Scripts   ########################
####################################################################
function prepare_scripts() {
  if [[ $step == "all" || $step == "0" || $step == "1" || $step == "3" || $step == "6" ]]; then
    echo -e "$I $(current_time) Preparing scripts..."
    echo -e "$I $(current_time) Removing old scripts..."
    rm -f $launchAnalysis_step1 $launchAnalysis_step3 $launchAnalysis_step6 $listSamplesToRun_HZZ $listSamplesToRun_Photon

    echo -e "$I $(current_time) Copying scripts and dataset lists..."
    cp ../../launchAnalysis.sh $launchAnalysis_step1
    cp ../../listSamplesToRun.txt $listSamplesToRun_HZZ
    cp ../../listSamplesToRun_InstrMET.txt $listSamplesToRun_Photon
  
    echo -e "$I $(current_time) Modifying scripts and dataset lists..."
    #First dataset list names
    sed -i "s|^  listDataset=\".*\"$|  listDataset=\"${instrMET_path}listSamplesToRun_diLeptonMCOnly.txt\"|" $launchAnalysis_step1
    sed -i "s|^  listDataset_InstrMET=\".*\"$|  listDataset_InstrMET=\"${instrMET_path}listSamplesToRun_InstrMET_GJetsAndQCDMCOnly.txt\"|" $launchAnalysis_step1
    #Then we make a copy of the launchAnalysis because the suffix will need to be different between step1 and step3
    if [ $step != "1" ]; then cp $launchAnalysis_step1 $launchAnalysis_step3; cp $launchAnalysis_step1 $launchAnalysis_step6; fi
    #Then we change the suffix according to the step
    sed -i "s|^  suffix=\".*\"$|  suffix=\"${suffix_step1_HZZ}\"|" $launchAnalysis_step1
    sed -i "s|^  suffix_InstrMET=\".*\"$|  suffix_InstrMET=\"${suffix_step1_InstrMET}\"|" $launchAnalysis_step1
    if [ $step != "1" ]; then sed -i "s|^  suffix_InstrMET=\".*\"$|  suffix_InstrMET=\"${suffix_step3_InstrMET}\"|" $launchAnalysis_step3; fi
    if [ $step != "1" ]; then sed -i "s|^  suffix_InstrMET=\".*\"$|  suffix_InstrMET=\"${suffix_step6_InstrMET}\"|" $launchAnalysis_step6; fi

    sed -i '/Bonzais-DY/b; /^Bonzais-/d' $listSamplesToRun_HZZ
    if [ $addQCD == "0" ]; then sed -i '/Bonzais-GJets/b; /^Bonzais-/d' $listSamplesToRun_Photon; fi
    if [ $addQCD == "1" ]; then sed -i '/Bonzais-GJets/b; /Bonzais-QCD/b; /^Bonzais-/d' $listSamplesToRun_Photon; fi
    echo -e "$I $(current_time) Done."
  fi

}

function backup_previousWeights() {
  if [[ (-f "${full_path}closureTest_weight_NVtx.root") || (-f "${full_path}closureTest_weight_pt.root") || (-f "${full_path}closureTest_lineshape_mass.root") ]]; then
    if ! [ "$step" == "0" ]; then
      backupForWeight="${full_path}backupForWeightForClosure_$(current_time)"
      mkdir -p $backupForWeight
      echo -e "$W The previous weight files (if not needed by the current step) have been put in a backup folder here: $backupForWeight"
    fi
    
    if [[ $step == "all" || $step == "1" || $step == "2" ]]; then #for the first steps we don't need weight files
      mv closureTest_weight_NVtx.root $backupForWeight
      mv closureTest_weight_pt.root $backupForWeight

    elif [[ $step == "3" || $step == "4" ]]; then #but for those steps, we need the 1st weight file, so we just move the second weight file
      mkdir -p $backupForWeight
      mv closureTest_weight_pt.root $backupForWeight
    
    fi
    if [[ $step == "all" || $step == "5" ]]; then #Just remove line shape if we reproduce it with 5 or all
      mkdir -p $backupForWeight
      mv ${full_path}closureTest_lineshape_mass.root $backupForWeight

    fi
  fi

}

####################################################################
######################   Waiting Functions   #######################
####################################################################
function check_number_of_remaining_jobs_to_send_from_bigSubmission() { #this is here in case a job failed to be sent in the first steps of big-submission... and so we wait for it
  #This function takes one argument: the suffix of the job where we want to do the check
  theSuffix=$1
  if [ -z "$theSuffix" ]; then
    echo -e "$E Error, you used the function 'check_number_of_remaining_jobs_to_send_from_bigSubmission' without specifying an argument!"
    return 0
  fi

  while [ $(wc -l < ${base_path}OUTPUTS/${theSuffix}/sendJobs_${theSuffix}.cmd) -gt 0 ]
  do
    echo -e "$I $(current_time) There are still $(wc -l < ${base_path}OUTPUTS/${theSuffix}/sendJobs_${theSuffix}.cmd) jobs to send for ${theSuffix}"
    sleep 60
  done
  if [ $(grep -c -e '^qsub ' $(ls -Art ${base_path}OUTPUTS/${theSuffix}/big-submission-*.err | tail -n 1) ) -gt 0 ]; then
    retryCounter=0
    while [ $(grep -c -e '^qsub ' $(ls -Art ${base_path}OUTPUTS/${theSuffix}/big-submission-*.err | tail -n 1) ) -gt 0 ]
    do
      echo -e "$W There are jobs that failed to be submitted in ${base_path}OUTPUTS/${theSuffix}. Let's wait a bit to see if they manage to be submitted"
      sleep 60
      if [ $retryCounter  == 7 ]; then
        echo -e "$E Big-submission didn't manage to send all jobs. We stop here!"
        send_mail
        return 0
      fi
      retryCounter=$((retryCounter+1))
    done
  fi

}

function check_if_jobs_are_done() {
  #This function takes three arguments: the suffix of the job where we want to do the check; the folder where we want to check the output; and the folder that we should use for comparison to know if there are remaining jobs or not.
  theSuffix="$1"
  outputFolderToCheck="$2"
  inputFolderToCompareTo="$3"
  if [[ (-z "$theSuffix") || (-z "$outputFolderToCheck") || (-z "$inputFolderToCompareTo") ]]; then
    echo -e "$E Error, you used the function 'check_if_jobs_are_done' without specifying three arguments!"
    return 0
  fi
  
  totalJobs=$(eval ls -1 ${base_path}OUTPUTS/${theSuffix}/${inputFolderToCompareTo} | wc -l)
  sleptTime=1  #don't make it start at 0
  while [ $(getRemainingJobs "$theSuffix" "$outputFolderToCheck" "$totalJobs") -gt 0 ]
  do
    echo -e "$I $(current_time) There are $(getRemainingJobs "$theSuffix" "$outputFolderToCheck" "$totalJobs") jobs remaining" 

    if (( $sleptTime % ($SLEEP_TIME_QSTAT*$CHECK_TIME/$SLEEP_TIME) == 0 ))
    then
      echo "Checking with qstat to see if there are still running/pending jobs, or if they crashed..."
      nofJobs=$(getNumJobsOnCE)
      echo -e "$I $(current_time) There are $nofJobs jobs running/pending on the cluster"
      if (( $nofJobs == 0 ))
      then
        echo -e "No jobs are running or pending in the grid, I guess some jobs failed!"
        if (( $(getRemainingJobs "$theSuffix" "$outputFolderToCheck" "$totalJobs") == 0 ))
        then
          echo "No, it's fine."
        else
          echo -e "$E Some jobs have failed! Exiting"
          exit 3
        fi
      fi
    fi

    sleep $SLEEP_TIME
    sleptTime=$((sleptTime + 1))
  done

}

function getRemainingJobs() {
  theSuffix="$1"
  outputFolderToCheck="$2"
  totalJobs="$3"
  jobsDone=$(eval ls -1 ${base_path}OUTPUTS/${theSuffix}/${outputFolderToCheck} | wc -l)
  echo $(($totalJobs-$jobsDone)) #number of running/remaining jobs
}

function fileIsFresh(){
  file=$1
  fileTimeInSeconds=$(timeout -s 9 3 ls -l --time-style=+%s $file|awk '{print $6}')
  currentTimeInSeconds=$(date +%s)
  let currentTimeInSeconds=$currentTimeInSeconds-$SLEEP_TIME_QSTAT

  if [ $fileTimeInSeconds -gt $currentTimeInSeconds ];then
    return 0
  else
    return 1
  fi
}

function getNumJobsOnCE(){
  nameOfJob=$1
  # Setting default for number of jobs
  nJobs=-1

  # NFS file that is used first
  qstatFile='/group/log/dumpOfFullQstat'

  # Checking if NFS file is accessible and not empty
  timeout -s 9 3 ls $qstatFile &> /dev/null
  if [ $? -eq 0 ] && [ $(wc -l $qstatFile|awk '{print $1}') -gt 0 ];then

    # Checking if NFS file has been updated recently
    if fileIsFresh $qstatFile;then
      if [ -z "$nameOfJob" ]; then
        nJobs=$(cat $qstatFile|grep -e Job_Owner|grep -c $USER)
      else
        nJobs=$(cat $qstatFile|grep -e Job_Owner|grep $nameOfJob|grep -c $USER)
      fi
    fi
  fi

  # If NFS is not accessible, or file is not fresh
  if [ $nJobs -eq -1 ];then

    # Doing a standard qstat
    if [ -z "$nameOfJob" ]; then
      nJobs=$(qstat -u $USER |grep $USER|wc -l)
    else
      nJobs=$(qstat -u $USER |grep $USER|grep $nameOfJob|wc -l)
    fi

    # If qstat command fails, sleep then start again
    if [ $? -ne 0 ];then
      sleep 20
      if [ -z "$nameOfJob" ]; then
        nJobs=$(qstat -u $USER |grep $USER|wc -l)
      else
        nJobs=$(qstat -u $USER |grep $USER|grep $nameOfJob|wc -l)
      fi

      # If qstat fails again, set it to 9999 so we know it failed
      if [ $? -ne 0 ];then
        let nJobs=9999
      fi

    fi
  fi

  echo $nJobs
}


####################################################################
#######################   Step 0 - cleaning   ######################
####################################################################
function cleaning() {
  if [[ $step == "all" || $step == "0" ]]; then
    echo -e "$I $(current_time) Cleaning all..."
    echo "a" | source $launchAnalysis_step1 0 HZZanalysis
    echo "a" | source $launchAnalysis_step1 0 InstrMET
    echo "a" | source $launchAnalysis_step3 0 InstrMET
    echo "a" | source $launchAnalysis_step6 0 InstrMET
  elif [[ $step == "1" ]]; then
    echo -e "$I $(current_time) Cleaning folders linked to step1..."
    echo "a" | source $launchAnalysis_step1 0 HZZanalysis
    echo "a" | source $launchAnalysis_step1 0 InstrMET
  elif [[ $step == "3" ]]; then
    echo -e "$I $(current_time) Cleaning folders linked to step3..."
    echo "a" | source $launchAnalysis_step3 0 InstrMET
  elif [[ $step == "6" ]]; then
    echo -e "$I $(current_time) Cleaning folders linked to step6..."
    echo "a" | source $launchAnalysis_step6 0 InstrMET
  fi
  echo -e "$I $(current_time) Cleaning done."
}

####################################################################
#########    Step 1 - Run On DiLepton and Photon MC Only    ########
####################################################################
function runOnDiLeptonAndPhotonMCOnly() {
  echo -e "$I $(current_time) Starting step 1: Run On DiLepton and Photon MC Only..."
 
  echo -e "$I $(current_time) Launching HZZanalysis analysis..."
  yes | source $launchAnalysis_step1 1 HZZanalysis $noLocalCopy $express
  if [ $? -eq 0 ]; then
    echo -e "$E Step 1 failed for HZZanalysis, exiting."
    send_mail
    return 0
  fi
  
  echo -e "$I $(current_time) Launching InstrMET analysis..."
  echo -e "$I $(current_time) Creating the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should run the closure test..."
  touch ${full_path}please_do_closure_test_when_running_InstrMETLooper
  yes | source $launchAnalysis_step1 1 InstrMET $noLocalCopy $express
  if [ $? -eq 0 ]; then
    echo -e "$E Step 1 failed for InstrMET, exiting."
    send_mail
    return 0
  fi
 
  echo -e "$I $(current_time) Waiting for the jobs from step 1 to be over..."
  sleep 60
  check_number_of_remaining_jobs_to_send_from_bigSubmission ${suffix_step1_HZZ}
  check_number_of_remaining_jobs_to_send_from_bigSubmission ${suffix_step1_InstrMET}

  outputFolderToCheck="OUTPUTS"
  inputFolderToCompareTo="JOBS/scripts"
  check_if_jobs_are_done "${suffix_step1_HZZ}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Jobs for ${suffix_step1_HZZ} are done, sending harvesting..."
  send_harvesting ${suffix_step1_HZZ} HZZanalysis ${launchAnalysis_step1}
  check_if_jobs_are_done "${suffix_step1_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Jobs for ${suffix_step1_InstrMET} are done, sending harvesting..."
  send_harvesting ${suffix_step1_InstrMET} InstrMET ${launchAnalysis_step1}

  echo -e "$I $(current_time) Waiting for the the harvesting to be over for both samples..."
  sleep 60
  mkdir -p ${base_path}OUTPUTS/${suffix_step1_HZZ}/MERGED
  mkdir -p ${base_path}OUTPUTS/${suffix_step1_InstrMET}/MERGED
  outputFolderToCheck="MERGED | grep -v output.*_Data.root "
  inputFolderToCompareTo="OUTPUTS | grep _0.root "
  check_if_jobs_are_done "${suffix_step1_HZZ}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  check_if_jobs_are_done "${suffix_step1_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"

  echo -e "$I $(current_time) Deleting the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should stop running the closure test..."
  rm -f ${full_path}please_do_closure_test_when_running_InstrMETLooper
  echo -e "$I $(current_time) The harvesting is done for both samples and can be found in ${suffix_step1_HZZ} and ${suffix_step1_InstrMET}." 
}


function send_harvesting() {
  #This function takes three arguments: the suffix of the job where we want to do the check; the folder where we want to check the output; and the launchAnalysis to use
  theSuffix=$1
  analysisType=$2
  launchAnalysis_stepX=$3
  if [[ (-z "$theSuffix") || (-z "$analysisType") ]]; then
    echo -e "$E Error, you used the function 'send_harvesting' without specifying two arguments!"
    return 0
  fi


  prepare_jobs_for_express > ${base_path}OUTPUTS/$theSuffix/harvesting.sh
  echo "echo \"y\" | sh $launchAnalysis_stepX 2 $analysisType" >> ${base_path}OUTPUTS/$theSuffix/harvesting.sh
  retryCounter=0
  while [ $retryCounter -lt 3 ]; do
    if qsub -q express -l walltime=00:30:00 -j oe -o ${base_path}OUTPUTS/$theSuffix/ ${base_path}OUTPUTS/$theSuffix/harvesting.sh 2>&1 | grep -q 'qsub'; then
      echo -e "$W Failed to submit to the grid, retry in 30s"
      retryCounter=$((retryCounter+1))
      sleep 30
    else
      echo -e "$I Harvesting for $analysisType submitted"
      retryCounter=10
    fi
  done
  if [ $retryCounter  == 3 ]; then
    echo -e "$E Failed 3 times to send jobs, exiting"
    send_mail
    return 0
  fi

}

function prepare_jobs_for_express() {
  echo "export INITDIR=$HZZ2L2NU_BASE"
  echo "cd \$INITDIR"
  echo "hostname ;"
  echo "date;"

}

####################################################################
#################   Step 2 - Compute Weight NVtx   #################
####################################################################
function computeWeightNVtx() {
  echo -e "$I $(current_time) Starting step 2: Compute Weight NVtx..."
  root -l -q "macroToComputeClosureTestWeights.C++(1, ${addQCD})"
  echo -e "$I $(current_time) Step 2 is done."

}


####################################################################
#######    Step 3 - Re Run On Photon MC With NVtx Weights    #######
####################################################################
function reRunOnPhotonMCWithNVtxWeights() {
  echo -e "$I $(current_time) Starting step 3: Re Run On Photon Data With NVtx Weights..."

  echo -e "$I $(current_time) Creating the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should run the closure test..."
  touch ${full_path}please_do_closure_test_when_running_InstrMETLooper
  echo -e "$I $(current_time) Launching InstrMET analysis..."
  yes | source $launchAnalysis_step3 1 InstrMET $noLocalCopy $express
  if [ $? -eq 0 ]; then
    echo -e "$E Step 1 failed for InstrMET, exiting."
    send_mail
    return 0
  fi

  echo -e "$I $(current_time) Waiting for the jobs from step 3 to be over..."
  sleep 60
  check_number_of_remaining_jobs_to_send_from_bigSubmission ${suffix_step3_InstrMET}

  outputFolderToCheck="OUTPUTS"
  inputFolderToCompareTo="JOBS/scripts"
  check_if_jobs_are_done "${suffix_step3_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Jobs for ${suffix_step3_InstrMET} are done, sending harvesting..."
  send_harvesting ${suffix_step3_InstrMET} InstrMET ${launchAnalysis_step3} 

  echo -e "$I $(current_time) Waiting for the the harvesting to be over..."
  sleep 60
  mkdir -p ${base_path}OUTPUTS/${suffix_step3_InstrMET}/MERGED
  outputFolderToCheck="MERGED | grep -v output.*_Data.root "
  inputFolderToCompareTo="OUTPUTS | grep _0.root "
  check_if_jobs_are_done "${suffix_step3_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Deleting the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should stop running the closure test..."
  rm -f ${full_path}please_do_closure_test_when_running_InstrMETLooper
  echo -e "$I $(current_time) The harvesting is done for Instr. MET sample and can be found in ${suffix_step3_InstrMET}." 
}


####################################################################
###############   Step 4 - Compute Final Weights Pt   ##############
####################################################################
function computeFinalWeightsPt() {
  echo -e "$I $(current_time) Starting step 4: Compute Final Weights Pt..."
  root -l -q "macroToComputeClosureTestWeights.C++(2, ${addQCD})"
  echo -e "$I $(current_time) Step 4 is done."

}

####################################################################
#############  Step 5 - Compute Mass LineShape Weights  ############
####################################################################
function computeMassLineShapeWeights() {
  echo -e "$I $(current_time) Starting step 5: Compute Mass LineShape Weights..."
  root -l -q "macroToComputeClosureTestWeights.C++(3, ${addQCD})"
  echo -e "$I $(current_time) Step 5 is done."

}

####################################################################
##  Step 6 - Re Run On Photon MC With All Weights And Lineshape ##
####################################################################
function ReRunOnPhotonMCWithAllWeightsAndLineshape() {
  echo -e "$I $(current_time) Starting step 6: Re Run On Photon MC With All Weights And Lineshape..."

  echo -e "$I $(current_time) Creating the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should run the closure test..."
  touch ${full_path}please_do_closure_test_when_running_InstrMETLooper
  echo -e "$I $(current_time) Launching InstrMET analysis..."
  yes | source $launchAnalysis_step6 1 InstrMET $noLocalCopy $express #Basically rerun step 3 :)
  if [ $? -eq 0 ]; then
    echo -e "$E Step 1 failed for InstrMET, exiting."
    send_mail
    return 0
  fi

  echo -e "$I $(current_time) Waiting for the jobs from step 6 to be over..."
  sleep 60
  check_number_of_remaining_jobs_to_send_from_bigSubmission ${suffix_step6_InstrMET}

  outputFolderToCheck="OUTPUTS"
  inputFolderToCompareTo="JOBS/scripts"
  check_if_jobs_are_done "${suffix_step6_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Jobs for ${suffix_step6_InstrMET} are done, sending harvesting..."
  send_harvesting ${suffix_step6_InstrMET} InstrMET ${launchAnalysis_step6}

  echo -e "$I $(current_time) Waiting for the the harvesting to be over..."
  sleep 60
  mkdir -p ${base_path}OUTPUTS/${suffix_step6_InstrMET}/MERGED
  outputFolderToCheck="MERGED | grep -v output.*_Data.root "
  inputFolderToCompareTo="OUTPUTS | grep _0.root "
  check_if_jobs_are_done "${suffix_step6_InstrMET}" "$outputFolderToCheck" "$inputFolderToCompareTo"
  echo -e "$I $(current_time) Deleting the file 'please_do_closure_test_when_running_InstrMETLooper' so the InstrMET Looper is aware he should stop running the closure test..."
  rm -f ${full_path}please_do_closure_test_when_running_InstrMETLooper
  echo -e "$I $(current_time) The harvesting is done for Instr. MET sample and can be found in ${suffix_step6_InstrMET}."
  echo -e "$I $(current_time) Step 6 is done."

}

####################################################################
###############         Step 7 - Closure Test         ##############
####################################################################
function closureTest() {
  echo -e "$I $(current_time) Starting step 7: Closure Test..."
  rm -f ${full_path}*png
  rm -f ${full_path}closureTestResults.root
  publication_time=$(current_time)
  mkdir -p ${full_path}closureTest_$publication_time
  root -l -q "macroToComputeClosureTestWeights.C++(4, ${addQCD})"
  mv ${full_path}*png ${full_path}closureTest_$publication_time/.
  mv ${full_path}closureTestResults.root ${full_path}closureTest_$publication_time/.
  echo -e "$I $(current_time) Plots from closure tests have been created in ${full_path}closureTest_$publication_time. Now let's publish them online..."
  mkdir -p ~/public_html
  chmod 755 ~/public_html
  mkdir -p ~/public_html/SHEARS_PLOTS
  rm -rf ~/public_html/SHEARS_PLOTS/closureTest_$publication_time
  mkdir -p ~/public_html/SHEARS_PLOTS/closureTest_$publication_time
  ln -s ${full_path}closureTest_$publication_time/*png ~/public_html/SHEARS_PLOTS/closureTest_${publication_time}/.
  cp ${base_path}Tools/index.php ~/public_html/SHEARS_PLOTS/closureTest_${publication_time}/.
  echo -e "$I Your plots are available in ~/public_html/SHEARS_PLOTS/closureTest_${publication_time}/, i.e. on http://homepage.iihe.ac.be/~$USER/SHEARS_PLOTS/closureTest_${publication_time}/"
  echo -e "$I $(current_time) Step 7 is done."

}

####################################################################
####################################################################
####################################################################
###############  #####  #######   #######  ###  ####  ##############
###############   ###   ######  #  ######  ###   ###  ##############
###############  # # #  #####  ###  #####  ###  # ##  ##############
###############  ## ##  ###           ###  ###  ## #  ##############
###############  #####  ####  #####  ####  ###  ###   ##############
###############  #####  ####  #####  ####  ###  ####  ##############
####################################################################
####################################################################
####################################################################

function main() {
  prepare_scripts
  backup_previousWeights
  cleaning #step 0 is embedded in this function
  if [[ $step == "all" || $step == "1" ]]; then runOnDiLeptonAndPhotonMCOnly; fi
  if [[ $step == "all" || $step == "2" ]]; then computeWeightNVtx; fi
  if [[ $step == "all" || $step == "3" ]]; then reRunOnPhotonMCWithNVtxWeights; fi
  if [[ $step == "all" || $step == "4" ]]; then computeFinalWeightsPt; fi
  if [[ $step == "all" || $step == "5" ]]; then computeMassLineShapeWeights; fi
  if [[ $step == "all" || $step == "6" ]]; then ReRunOnPhotonMCWithAllWeightsAndLineshape; fi
  if [[ $step == "all" || $step == "7" ]]; then closureTest; fi
  send_mail
}

# Steps name
declare -A stepName
stepName[all]='Clean - run on DiLepton/Photon MC - compute first weight for Nvtx - rerun on photon MC with this weight - compute final weight for Pt - compute lineshape - rerun on photon MC with all weights and lineshape - run closure test'
stepName[0]='Cleaning'
stepName[1]='Run On DiLepton and Photon MC Only'
stepName[2]='Compute Weight NVtx'
stepName[3]='Re Run On Photon MC With NVtx Weights'
stepName[4]='Compute Final Weights Pt'
stepName[5]='Compute Mass LineShape Weights'
stepName[6]='Re Run On Photon MC With All Weights And Lineshape'
stepName[7]='Closure Test'

load_options
for arg in "$@"
do
  case $arg in -h|-help|--help) usage  ; exit 0 ;; esac
  case $arg in -e|-express|--express) express="--express"  ;; esac
  case $arg in -nlc|-noLocalCopy|--noLocalCopy) noLocalCopy="--noLocalCopy"  ;; esac
  case $arg in all|0|1|2|3|4|5|6|7) step="$arg"  ;; esac
  case $arg in --addQCD|--QCD) addQCD=1  ;; esac
done

check_running_process

print_introduction_text
echo -e "$W Do you wish to compute Instr. MET weights with those options? [${RED}N${DEF}/${GREEN}y${DEF}]"
read answer
if [[ $answer == "y" ]];
then
  mkdir -p ${base_path}OUTPUTS 

  echo -e "$I Script launched! The log are available here: tail -f $YEL ${logFile} $DEF"
  echo -e "$I Open it with 'tail -f' for realtime update or with 'less -R' to benefit from the colour output."

  main &> $logFile &


fi
