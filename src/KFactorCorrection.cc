#include <KFactorCorrection.h>

#include <stdexcept>
#include <sstream>
#include <string>

#include <FileInPath.h>
#include <Logger.h>

#include <TFile.h>
#include <TLorentzVector.h>


KFactorCorrection::KFactorCorrection(Dataset &dataset, Options const &)
    : genPartPt_{dataset.Reader(), "GenPart_pt"},
      genPartEta_{dataset.Reader(), "GenPart_eta"},
      genPartPhi_{dataset.Reader(), "GenPart_phi"},
      genPartMass_{dataset.Reader(), "GenPart_mass"},
      genPartStatus_{dataset.Reader(), "GenPart_status"},
      genPartStatusFlags_{dataset.Reader(), "GenPart_statusFlags"} {

  auto const settingsNode = dataset.Info().Parameters()["k_factor"];

  if (settingsNode and not settingsNode.IsNull()) {
    enabled_ = true;
    auto const typeLabel = settingsNode.as<std::string>();

    if (typeLabel != "ggF") {
      std::ostringstream message;
      message << "Unknown type \"" << typeLabel << "\" for k factor.";
      throw std::runtime_error(message.str());
    }
  } else
    enabled_ = false;

  if (enabled_) {
    LOG_DEBUG << "Will apply k factors.";
    TDirectory* currentDirectory = gDirectory;
    TFile* kFactorFile = TFile::Open(FileInPath::Resolve(
      "corrections/Kfactor_Collected_"
      "ggHZZ_2l2l_NNLO_NNPDF_NarrowWidth_13TeV.root").c_str());
    currentDirectory->cd();
    kfactors_.reserve(9);
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_Nominal")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_QCDScaleDn")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_QCDScaleUp")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_PDFScaleDn")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_PDFScaleUp")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_PDFReplicaDn")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_PDFReplicaUp")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_AsDn")));
    kfactors_.emplace_back(*(kFactorFile->Get<TSpline3>("sp_kfactor_AsUp")));
    kFactorFile->Close();
  }
  else
    LOG_DEBUG << "Will not apply k factors.";
}


double KFactorCorrection::HiggsMass() const {
  if (enabled_) {
    TLorentzVector higgs;
    int numberOfLepton = 0;

    for (int i = 0; i < int(genPartPt_.GetSize()); i++) {
      // Status: 1=stable
      // flags bits are: 0 : isPrompt, 8 : fromHardProcess
      if (genPartStatus_[i] != 1 || (genPartStatusFlags_[i] & 1 << 0) == 0 ||
          (genPartStatusFlags_[i] & 1 << 8) == 0 ) 
        continue;

      TLorentzVector lepton;
      lepton.SetPtEtaPhiM(genPartPt_[i], genPartEta_[i], genPartPhi_[i],
        genPartMass_[i]);
      higgs += lepton;
      numberOfLepton++;
    }

    if (numberOfLepton != 4) {
      std::ostringstream message; 
      message << "Found " << numberOfLepton
          << " generator-level leptons while 4 is expected. Higgs boson is not "
          "reconstructed correctly.";
      throw std::runtime_error(message.str());
    }
    return higgs.M();
  }
  else
    return 0.;
}


double KFactorCorrection::NominalWeight() const {
  if (enabled_)
    return kfactors_.at(0).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightRenScaleDown() const {
  if (enabled_)
    return kfactors_.at(1).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightRenScaleUp() const {
  if (enabled_)
    return kfactors_.at(2).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightFacScaleDown() const {
  if (enabled_)
    return kfactors_.at(3).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightFacScaleUp() const {
  if (enabled_)
    return kfactors_.at(4).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightPdfDown() const {
  if (enabled_)
    return kfactors_.at(5).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightPdfUp() const {
  if (enabled_)
    return kfactors_.at(6).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightAsMzDown() const {
  if (enabled_)
    return kfactors_.at(7).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::WeightAsMzUp() const {
  if (enabled_)
    return kfactors_.at(8).Eval(HiggsMass());
  else
    return 1.;
}

double KFactorCorrection::RelWeight(int variation) const {
  if (enabled_) {
    double mass = HiggsMass();
    return kfactors_.at(variation).Eval(mass) / kfactors_.at(0).Eval(mass);
  }
  else
    return 1.;
}

std::string_view KFactorCorrection::VariationName(int variation) const {
  if (enabled_) {
    switch (variation){
    case 0:
      return "";
    case 1:
      return "kfactor_qcd_nnlo_gg_renorm_down";
    case 2:
      return "kfactor_qcd_nnlo_gg_renorm_up";
    case 3:
      return "kfactor_qcd_nnlo_gg_factor_down";
    case 4:
      return "kfactor_qcd_nnlo_gg_factor_up";
    case 5:
      return "kfactor_qcd_nnlo_gg_pdf_down";
    case 6:
      return "kfactor_qcd_nnlo_gg_pdf_up";
    case 7:
      return "kfactor_qcd_nnlo_gg_asmz_down";
    case 8:
      return "kfactor_qcd_nnlo_gg_asmz_up";
    default:
      return WeightBase::VariationName(variation);
    }
  }
  else return WeightBase::VariationName(variation);
}
